package org.batsunov;

import java.util.Objects;

public class Circle implements Shape {

    private int radius;

    public Circle(int radius) { this.radius = radius; }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public int getRadius() {
        return this.radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return radius == circle.radius;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public int getPerimeter() {
        return (int)(2 * Math.PI * radius);
    }

    @Override
    public int getArea() {
        return (int)(Math.PI * radius * radius);
    }
}
