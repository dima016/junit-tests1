package org.batsunov;

import java.util.List;

public class ColorShapePrinter {
    public static <T extends Shape & Colorable> void print(List<T> shapes) {
        for (T shape : shapes) {
            System.out.printf("T: %s, P: %d, S: %d, C: %s\n",
                    shape.getClass().getSimpleName(),
                    shape.getPerimeter(),
                    shape.getArea(),
                    shape.getColor()
            );
        }
    }

    public static void main(String[] args) {
        print(List.of(
                new ColorCircle(5, "red"),
                new ColorTriangle(5, 6, 30, "white")
        ));
    }
}
