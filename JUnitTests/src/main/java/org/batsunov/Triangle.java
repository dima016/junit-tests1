package org.batsunov;

public class Triangle implements Shape {

    private int a;
    private int b;
    private double angle;

    public Triangle(int a, int b, int angle) {
        this.a = a;
        this.b = b;
        this.angle = Math.toRadians(angle);
    }

    public void setA(int a) {
        this.a = a;
    }

    public void setB(int b) {
        this.b = b;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public double getAngle() {
        return angle;
    }

    @Override
    public int getPerimeter() {
        return a + b + getC();
    }

    @Override
    public int getArea() {
        int p = getHalfPerimeter();
        return (int) Math.sqrt(p * (p - a) * (p - b) * (p - getC()));
    }


    @Override
    public String toString() {
        return "Triangle{" +
                "a=" + a +
                ", b=" + b +
                ", angle=" + angle +
                '}';
    }

    private int getHalfPerimeter() {
        return getPerimeter() / 2;
    }

    private int getC() {
        return (int) Math.sqrt(a * a + b * b - 2 * a * b * Math.cos(angle));
    }
}
