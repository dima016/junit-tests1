package org.batsunov;

public interface Shape {
    int getPerimeter();
    int getArea();
}
