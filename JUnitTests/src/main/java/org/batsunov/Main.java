package org.batsunov;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {

    // 2.1
    @SafeVarargs
    public static <T> List<T> merge1(List<T>... lists) {
        List<T> result = new ArrayList<>();
        for (List<T> list : lists) {
            result.addAll(list);
        }
        return result;
    }

    // 2.2
    public static List<Object> merge2(List<?>... lists) {
        List<Object> result = new ArrayList<>();
        for (List<?> list : lists) {
            result.addAll(list);
        }
        return result;
    }

    // 2.3
    @SafeVarargs
    public static List<Comparable<?>> merge3(List<? extends Comparable<?>>... lists) {
        List<Comparable<?>> result = new ArrayList<>();
        for (List<? extends Comparable<?>> list : lists) {
            result.addAll(list);
        }
        return result;
    }

    // 2.4
    @SafeVarargs
    public static <T extends Comparable<T>> List<T> merge4(List<T>... lists) {
        List<T> result = new ArrayList<>();
        for (List<T> list : lists) {
            result.addAll(list);
        }
        Collections.sort(result);
        return result;
    }

    public static void main(String[] args) {
        System.out.println("2.1\n====");

        List<String> listA = new ArrayList<>();
        listA.add("One");
        listA.add("Two");
        List<String> listB = new ArrayList<>();
        listB.add("Three");
        listB.add("Four");

        List<String> merged1 = merge1(listA, listB);
        merged1.forEach(System.out::println);

        System.out.println("\n2.2\n====");

        List<Integer> listC = new ArrayList<>();
        listC.add(123);

        merge2(listA, listB, listC).forEach(System.out::println);

        System.out.println("\n2.3\n====");

        List<Integer> listD = new ArrayList<>();
        listD.add(123);
        listD.add(321);
        List<BigDecimal> listE = new ArrayList<>();
        listE.add(new BigDecimal(20));
        listE.add(new BigDecimal(10));

        merge3(listD, listE).forEach(System.out::println);

        System.out.println("\n2.4\n====");

        List<BigInteger> listF = new ArrayList<>();
        listF.add(BigInteger.TWO);
        listF.add(BigInteger.ONE);
        List<BigInteger> listH = new ArrayList<>();
        listH.add(BigInteger.ZERO);
        listH.add(BigInteger.TEN);

        merge4(listF, listH).forEach(System.out::println);
    }
}
