package org.batsunov;

public interface Colorable {
    String getColor();
}
