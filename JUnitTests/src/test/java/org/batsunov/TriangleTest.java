package org.batsunov;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TriangleTest {

    Triangle triangle;


    @Before
    public void setTriangles() {
         triangle = new Triangle(10, 35, 60);
    }


    @Test
    public void shouldReturnCorrectRadiansAngel() {
        assertEquals(Math.toRadians(60), triangle.getAngle(), 0.01);
    }

    @Test
    public void shouldNotReturnZeroIfCorrectDate() {
        assertNotEquals(0,triangle.getArea());
    }

    @Test
    public void shouldReturnSameColorWhichBeInitialize() {
        ColorTriangle colorTriangle = new ColorTriangle(1,2,30,"blue");

        assertEquals("blue",colorTriangle.getColor());
    }

}
