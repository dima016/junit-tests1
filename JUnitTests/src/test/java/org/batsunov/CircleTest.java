package org.batsunov;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircleTest {

    Circle circle;
    ColorCircle colorCircle;

    @Before
    public void setCircles(){
         circle = new Circle(0);
         colorCircle = new ColorCircle(0, "red");
    }



    @Test
    public void shouldReturnZeroIfRadiusIsZero() {

        //test just circle
        assertEquals(0, circle.getArea());
        assertEquals(0, circle.getPerimeter());

        //test Color circle
        assertEquals(0, colorCircle.getArea());
        assertEquals(0, colorCircle.getPerimeter());
    }


    @Test
    public void shouldReturnSameColorWhichBeInitialize() {

        assertEquals("red",colorCircle.getColor());
    }
}
