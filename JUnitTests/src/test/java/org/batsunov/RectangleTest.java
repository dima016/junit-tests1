package org.batsunov;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RectangleTest {

    @Test
    public void shodReturnZeroAreaIfWidthOrHeightIsZero() {
        Rectangle rectangleZeroWidth = new Rectangle(0, 10);
        Rectangle rectangleZeroHeight = new Rectangle(10, 0);

        //Zero Width
        assertEquals(0, rectangleZeroWidth.getArea());

        //Zero Height
        assertEquals(0, rectangleZeroHeight.getArea());
    }


    @Test
    public void shouldReturnZeroPerimeterIfWidthAndHeightZero() {

        Rectangle rectangleZeroWidthAndHeight = new Rectangle(0, 0);

        assertEquals(0, rectangleZeroWidthAndHeight.getPerimeter());
    }
}
